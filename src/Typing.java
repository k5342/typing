import javax.swing.*;
import javax.swing.Timer;
import javax.imageio.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;

public class Typing {
	
	public static void main(String[] args) {
		Mainframe frame = new Mainframe("TypingGame!!!");
		frame.setVisible(true);
	}

}

class Mainframe extends JFrame{
	
	Mainframe(String str){
		this.setTitle(str);
		this.setSize(750, 480);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(new MainPanel());
	}
	
}

class MainPanel extends JPanel{
	private CardLayout layout;
	private Game       game;
	private int        pos, fpos, ppos, ac, keys, alls;
	private long       time;
	public  long       start, end;
	MainPanel(){
		layout = new CardLayout();
		this.setLayout(layout);
		this.add(new TitlePanel());
		this.add(new MenuPanel());
		this.add(game = new Game());
		this.add(new ResultPanel());
	}
	
	class TitlePanel extends JPanel implements ActionListener{
		JPanel bpanel;
		JLabel    lbl;
		JButton   btn;
		
		TitlePanel(){
			lbl    = new JLabel("TypingGame!!!", JLabel.CENTER);
			bpanel = new JPanel();
			
			btn    = new JButton("Start");
			btn.setBounds((750-500)/2, 100, 500, 35);
			btn.addActionListener(this);
			
			bpanel.setLayout(null);
			bpanel.add(btn);
			
			lbl.setFont(new Font("Meiryo", Font.PLAIN, 40));
			
			this.setLayout(new GridLayout(2,1));
			this.add(lbl);
			this.add(bpanel);
		}
		
		public void actionPerformed(ActionEvent ae) {
			layout.next(this.getParent());
		}
	}
	
	class MenuPanel extends JPanel implements ActionListener{
		JPanel bpanel;
		JLabel    lbl;
		JButton   btn;
		
		MenuPanel(){
			lbl    = new JLabel("モードを選んでください", JLabel.CENTER);
			bpanel = new JPanel();
			
			btn    = new JButton("Single");
			btn.setBounds((750-500)/2, 100, 500, 35);
			btn.addActionListener(this);
			
			bpanel.setLayout(null);
			bpanel.add(btn);
			
			lbl.setFont(new Font("Meiryo", Font.PLAIN, 40));
			
			this.setLayout(new GridLayout(2,1));
			this.add(lbl);
			this.add(bpanel);
		}
		
		public void actionPerformed(ActionEvent ae) {
			layout.next(this.getParent());
			start = 0;
			end   = 0;
			ac    = 0;
			pos   = 0;
			fpos  = 0;
			ppos  = 0;
			keys  = 0;
			alls  = 0;
			time  = 0;
			game.setFocus();
		}
	}
	
	class Game extends JPanel{
		private JPanel   txtpanel;
		private String   typed, question, tmpTyped;
		private Question q;
		private Timer    gametimer;
		private long     atmiss;
		private long     count;
		private boolean  missed;
		public   int     max;
		BufferedImage    background;
		
		Game(){
			max        = 13;
			txtpanel   = new TxtPanel();
			txtpanel.setLayout(null);
			
			typed      = new String();
			tmpTyped   = new String();
			q          = new Question();
			
			time       = 0;
			gametimer  = new Timer(10, new GameTimer());
			
			pos        = 0;
			fpos       = 0;
			ppos       = 0;
			ac         = 0;
			
			keys       = 0;
			alls       = 0;
			
			atmiss     = -100;
			count      = 0;
			
			try{
				background = ImageIO.read(new FileInputStream("background.png"));
			}catch(IOException e){
				System.out.println("IO Error");
				System.exit(1);
			}
			
			q.init();
			
			this.setLayout(new GridLayout(1,1));
			this.add(txtpanel);
		}
		
		public void setFocus(){
			this.txtpanel.requestFocusInWindow();
		}
		
		class TxtPanel extends JPanel{
			
			Graphics2D g2;
			int offset;
			String[] current;
			
			TxtPanel(){
				this.setFocusable(true);
				
				this.addKeyListener(new KeyListener() {
					public void keyPressed(KeyEvent e){}
					public void keyReleased(KeyEvent e){}
					public void keyTyped(KeyEvent e){
						if (ac == 0 && pos == 0){
							start = System.nanoTime();	//1問目の入力開始でタイマスタート
							gametimer.start();
						}
						
						tmpTyped += e.getKeyChar();
						
						missed = true;
						
						offset = 0;

						if (fpos+1 < q.getF().length()){
							if (q.getF().charAt(fpos+1) == 'ぁ' ||
								q.getF().charAt(fpos+1) == 'ぃ' ||
								q.getF().charAt(fpos+1) == 'ぅ' ||
								q.getF().charAt(fpos+1) == 'ぇ' ||
								q.getF().charAt(fpos+1) == 'ぉ' ||
								q.getF().charAt(fpos+1) == 'ゃ' ||
								q.getF().charAt(fpos+1) == 'ゅ' ||
								q.getF().charAt(fpos+1) == 'ょ'){
								offset = 1;
							}
						}
						
						current = hiragana.get(""+q.getF().substring(fpos,fpos+1+offset));
						if (current == null){
							offset = 0;
							current = hiragana.get(""+q.getF().substring(fpos,fpos+1+offset));
						}
						System.out.println(hiragana.get(""+q.getF().substring(fpos,fpos+1+offset))[0]);
						if (fpos+2 < q.getF().length()){
							if (q.getF().charAt(fpos) == 'っ'){
								current = hiragana.get(""+q.getF().substring(fpos+1,fpos+2));
								current = Arrays.copyOf(current, current.length);
								for (int i = 0; i < current.length; i++){
									current[i] = ""+current[i].charAt(0);
								}
							}
						}
						System.out.println(fpos);
						
						for(String tmp : current){
							System.out.println(tmp);
							for (int i = 0; i < tmpTyped.length(); i++){
								if (tmp.charAt(i) == tmpTyped.charAt(i)){
									
									if (ppos <= i){
										typed += e.getKeyChar();
										ppos++;
										keys++;
										missed = false;
									}
									if (i == tmp.length()-1){
										pos++;
										fpos += 1+offset;
										ppos = 0;
										tmpTyped = "";
									}
								}else{
									break;
								}
							}
						}
						
						if (missed){
							atmiss = count;
							tmpTyped = tmpTyped.substring(0, tmpTyped.length()-1);
						}
							
						if (q.getF().length() <= fpos){
							pos = 0;
							fpos = 0;
							ppos = 0;
							ac++;
							typed = "";
							q.reset();
						}
						if (ac >= max){
							end = System.nanoTime();
							layout.next(Game.this.getParent());
							gametimer.stop();
						}
						alls++;
						repaint();
					}
				});
			}
			
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g2 = (Graphics2D)g;
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, 
									RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				
				g2.drawImage(background, null, 0, 0);
				
				g2.setColor(Color.red);
				if (count - atmiss < 15 && start > 0){
					g2.fillRect(0,   33, 750,  15);
					g2.fillRect(0,   33,  15, 203);
					g2.fillRect(730, 33,  15, 203);
					g2.fillRect(0,  221, 750,  15);
				}
				
				g2.setFont(new Font("Meiryo", Font.BOLD, 15));
				g2.setColor(Color.white);
				g2.drawString(""+ac+"/"+max+"", 670, 210);
				
				//問題文
				g2.setFont(new Font("Meiryo", Font.PLAIN, 25));
				g2.setColor(Color.white);
				g2.drawString(q.getJ(), 35, 125);
				
				//よみがな
				g2.setFont(new Font("Meiryo", Font.PLAIN, 21));
				g2.setColor(new Color(190,190,190));
				g2.drawString(q.getF(), 35, 90);
				
				//アルファベット
				g2.setFont(new Font("Meiryo", Font.BOLD, 23));
				g2.setColor(new Color(230,230,230));
				g2.drawString(q.getE(), 35, 165);
				
				//正解したやつのアルファベット
				g2.setColor(new Color(80, 158, 250));
				g2.drawString(typed, 35, 165);
				
				g2.setColor(Color.black);
				
				//経過時間表示
				g2.drawString(String.format("Time: %.2f", (double)(time)/10e+8), 35, 305);

				//データ表示
				g2.drawString("Miss: " + (alls-keys), 250, 305); //ミス
				
				g2.getPaint();
			}
			
		}
		
		class GameTimer implements ActionListener{
			
			public void actionPerformed(ActionEvent e) {
				time = System.nanoTime()-start;
				count++;
				repaint();
			}
			
		}
	}
	
	class ResultPanel extends JPanel implements ActionListener{
		JPanel bpanel;
		JButton   btn;
		
		Graphics2D g2;
		
		ResultPanel(){
			bpanel = new JPanel();
			
			btn    = new JButton("おわる");
			btn.setBounds((730-500)/2, 100, 500, 35);
			btn.addActionListener(this);
			
			bpanel.setLayout(null);
			bpanel.add(btn);
			
			this.setLayout(new GridLayout(2,1));
			this.add(new RPanel());
			this.add(bpanel);
		}
		
		class RPanel extends JPanel{
			protected void paintComponent(Graphics g){
				super.paintComponent(g);
				
				g2 = (Graphics2D)g;
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, 
									RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				
				g2.setFont(new Font("Meiryo", Font.BOLD, 24));
				g2.setColor(Color.black);
				g2.drawString("打鍵: "+(float)keys/((float)(end-start)/10e+8)+"/秒", 30, 110);
				g2.drawString("正確度: "+(((float)keys/alls)*100)+"%", 30, 150);
				g2.getPaint();
			}
		}
		
		public void actionPerformed(ActionEvent ae) {
			layout.next(this.getParent());
		}
	}
	
	private HashMap<String, String[]> hiragana = new HashMap<String, String[]>()
	{{
		put("あ",new String[]{"a","wu"});
		put("い",new String[]{"i","yi"});
		put("う",new String[]{"u","whu"});
		put("え",new String[]{"e"});
		put("お",new String[]{"o"});
		put("か",new String[]{"ka","ca"});
		put("き",new String[]{"ki"});
		put("く",new String[]{"ku","cu"});
		put("け",new String[]{"ke"});
		put("こ",new String[]{"ko","co"});
		put("さ",new String[]{"sa"});
		put("し",new String[]{"si","shi","ci"});
		put("す",new String[]{"su"});
		put("せ",new String[]{"se","ce"});
		put("そ",new String[]{"so"});
		put("た",new String[]{"ta"});
		put("ち",new String[]{"ti","chi"});
		put("つ",new String[]{"tu","tsu"});
		put("て",new String[]{"te"});
		put("と",new String[]{"to"});
		put("な",new String[]{"na"});
		put("に",new String[]{"ni"});
		put("ぬ",new String[]{"nu"});
		put("ね",new String[]{"ne"});
		put("の",new String[]{"no"});
		put("は",new String[]{"ha"});
		put("ひ",new String[]{"hi"});
		put("ふ",new String[]{"hu","fu"});
		put("へ",new String[]{"he"});
		put("ほ",new String[]{"ho"});
		put("ま",new String[]{"ma"});
		put("み",new String[]{"mi"});
		put("む",new String[]{"mu"});
		put("め",new String[]{"me"});
		put("も",new String[]{"mo"});
		put("や",new String[]{"ya"});
		put("ゆ",new String[]{"yu"});
		put("いぇ",new String[]{"ye"});
		put("よ",new String[]{"yo"});
		put("ら",new String[]{"ra"});
		put("り",new String[]{"ri"});
		put("る",new String[]{"ru"});
		put("れ",new String[]{"re"});
		put("ろ",new String[]{"ro"});
		put("わ",new String[]{"wa"});
		put("うぃ",new String[]{"wi"});
		put("うぇ",new String[]{"we"});
		put("を",new String[]{"wo"});
		put("ん",new String[]{"nn"});
		put("が",new String[]{"ga"});
		put("ぎ",new String[]{"gi"});
		put("ぐ",new String[]{"gu"});
		put("げ",new String[]{"ge"});
		put("ご",new String[]{"go"});
		put("ざ",new String[]{"za"});
		put("じ",new String[]{"zi"});
		put("ず",new String[]{"zu"});
		put("ぜ",new String[]{"ze"});
		put("ぞ",new String[]{"zo"});
		put("だ",new String[]{"da"});
		put("じ",new String[]{"zi","ji"});
		put("づ",new String[]{"du"});
		put("で",new String[]{"de"});
		put("ど",new String[]{"do"});
		put("ば",new String[]{"ba"});
		put("び",new String[]{"bi"});
		put("ぶ",new String[]{"bu"});
		put("べ",new String[]{"be"});
		put("ぼ",new String[]{"bo"});
		put("ぱ",new String[]{"pa"});
		put("ぴ",new String[]{"pi"});
		put("ぷ",new String[]{"pu"});
		put("ぺ",new String[]{"pe"});
		put("ぽ",new String[]{"po"});
		put("うぁ",new String[]{"wha"});
		put("うぃ",new String[]{"whi"});
		put("うぇ",new String[]{"whe"});
		put("うぉ",new String[]{"who"});
		put("きゃ",new String[]{"kya"});
		put("きぃ",new String[]{"kyi"});
		put("きゅ",new String[]{"kyu"});
		put("きぇ",new String[]{"kye"});
		put("きょ",new String[]{"kyo"});
		put("しゃ",new String[]{"sya","sha"});
		put("しぃ",new String[]{"syi"});
		put("しゅ",new String[]{"syu","shu"});
		put("しぇ",new String[]{"sye","she"});
		put("しょ",new String[]{"syo","sho"});
		put("ちゃ",new String[]{"tya","cha"});
		put("ちぃ",new String[]{"tyi","chi"});
		put("ちゅ",new String[]{"tyu","chu"});
		put("ちぇ",new String[]{"tye","che"});
		put("ちょ",new String[]{"tyo","cho"});
		put("にゃ",new String[]{"nya"});
		put("にぃ",new String[]{"nyi"});
		put("にゅ",new String[]{"nyu"});
		put("にぇ",new String[]{"nye"});
		put("にょ",new String[]{"nyo"});
		put("ひゃ",new String[]{"hya"});
		put("ひぃ",new String[]{"hyi"});
		put("ひゅ",new String[]{"hyu"});
		put("ひぇ",new String[]{"hye"});
		put("ひょ",new String[]{"hyo"});
		put("みゃ",new String[]{"mya"});
		put("みぃ",new String[]{"myi"});
		put("みゅ",new String[]{"myu"});
		put("みぇ",new String[]{"mye"});
		put("みょ",new String[]{"myo"});
		put("りゃ",new String[]{"rya"});
		put("りぃ",new String[]{"ryi"});
		put("りゅ",new String[]{"ryu"});
		put("りぇ",new String[]{"rye"});
		put("りょ",new String[]{"ryo"});
		put("ぁ",new String[]{"xa","la"});
		put("ぃ",new String[]{"xi","li"});
		put("ぅ",new String[]{"xu","lu"});
		put("ぇ",new String[]{"xe","le"});
		put("ぉ",new String[]{"xo","lo"});
		put("ゃ",new String[]{"xya","lya"});
		put("ゅ",new String[]{"xyu","lyu"});
		put("ょ",new String[]{"xyo","lyo"});
		put("っ",new String[]{"xtu","ltu"});
		put("ー",new String[]{"-"});
		put("・",new String[]{"."});
		put(",",new String[]{","});
		put("。",new String[]{"."});
		put("、",new String[]{","});
	}};
	
}
class Question{
	String   etext;
	String   ftext;
	String   jtext;
	String[] q;
	File     file;
	int      max, now, pos;
	
	Question(){
		etext           = new String();
		ftext           = new String();
		jtext           = new String();
		q               = new String[1000];
		
		pos   = 0;
		
		try{
			BufferedReader br = new BufferedReader(new FileReader(new File("q.txt").getAbsolutePath()));
			
			String str= null;
			max = 0;
			while((str = br.readLine()) != null){
				q[max]=str;
				max++;
			}
			
			br.close();
			
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void init(){
		this.reset();
	}
	public void reset(){
		//新しい問題を探す
		now   = (int)(Math.random()*getMax());
		jtext = q[now].split(",", 3)[0];
		ftext = q[now].split(",", 3)[1];
		etext = q[now].split(",", 3)[2];
	}
	
	public String getE(){
		return etext;
	}
	
	public String getF(){
		return ftext;
	}
	
	public String getJ(){
		return jtext;
	}
	
	private int getMax(){
		return max;
	}
	
}